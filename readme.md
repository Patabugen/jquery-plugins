# About
This is a collection of jQuery plugins I've created and released.

For more details, please see the demo page in each folder.

# Contents
### jquery.loading
A small, simple plugin to make it really easy to notify your user that some content is loading.
[Demo](http://pata.cat/tools/jquery-plugins/loading/example.html "Demo")

## jquery.select-icon
Replace a boring drop-down select element with fancy pants icons.
[Demo](http://pata.cat/tools/jquery-plugins/select-icon/example.html "Demo")

# Contact
You can contact me via my web page [Patabugen.co.uk](http://www.patabugen.co.uk/contact "Contact")