(function( $ ){
	
	$.fn.selectIcon = function(options)
	{
		var html = '';
		html += "<div id='icon-list-" + this.attr('id') + "' class='select_icon_box'>";
		$('#' + this.attr('id') + ' option').each(function(index, item){
			var val = $(this).val();
			var text = $(this).text();
			var parentId = $(this).parent().attr('id');	
			var id = parentId + "-item-" + index;
			
			html += "<div ";
			if($(this).is(':selected')){
				html +=		" class='select_icon_item select_icon_item_selected'";
			}else{
				html +=		" class='select_icon_item'";
			}
				html +=	" style='";
			if (options.width != undefined) {
				html +=	"width: " + options.width + "'";
			}
			html += "'" // End of style='' attribute

			html += 	" data-parent='" + parentId + "'";
			html += 	" data-value='" + val +  "'";
				html += ">"
				html += "<img "
				html += 	" src='" + options.icons[val] + "'";
				html += 	" alt='" + val +  "'";
				html +=		" id='" + id + "'";
				html +=	"/>";
				html += "<div>" + text + "</div>";
			html += "</div>";
		});
		this.hide();
		html += "</div>";
		this.before(html);
		// Make everything inside the div clickable
		$('#icon-list-' + this.attr('id') + ' .select_icon_item').on('click', function(){
			// Get the IDs from the data attributes
			var val = $(this).attr('data-value');
			var parentId = $(this).attr('data-parent');

			// Grab some useful DOM Objects
			var select = $('#' + parentId);
			var option = select.children('option[value="' + val +  '"]');
			var clickedIcon = $(this);

			// Un-select all in the dropdown
			select.children('option').removeAttr('selected');
			// Select the one which has been clicked
			option.attr('selected', 'selected');
			// Update the Icons classes for visual selection
			clickedIcon.siblings().removeClass('select_icon_item_selected');
			clickedIcon.addClass('select_icon_item_selected');

			// Trigger a Change in the Select
			select.val(option.val());
			select.change();
		});
	}
}) ( jQuery );
