(function($) {
		
	$.fn.loading = function(options) {
		this.html($.fn.loadingHtml(this, options));
	}	

	$.fn.loadingHtml = function(target, options) {
		if ( options == undefined) {
			options = {};
		}
		if (options.text == undefined) {
			options.text = 'Loading';
		}
		if (options.imageSrc == undefined) {
			options.imageSrc = 'data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH+GkNyZWF0ZWQgd2l0aCBhamF4bG9hZC5pbmZvACH5BAAKAAAAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQACgABACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkEAAoAAgAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkEAAoAAwAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkEAAoABAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQACgAFACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQACgAGACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAAKAAcALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==';
			options.imageWidth = 16;
			options.imageHeight = 16;
		}
		if (options.width == undefined) {
			// Go only as wide as the target object, but not wider than our source image
			options.width = target.width();
			if (options.width > options.imageWidth) {
				options.width = options.imageWidth + 'px';
			}
		}
		if (options.containerHeight == undefined) {
			options.containerHeight = target.height();
			if (options.containerHeight < options.imageHeight) {
				options.containerHeight = options.imageHeight + 'px';
			}
		}
		if (options.containerWidth == undefined) {
			options.containerWidth = target.width();
		}
		var html = '<div style="text-align: center; height: ' + options.containerHeight + 'px;';
			html += 'background: rgba(0, 0, 0, 0.05);';
			html += 'padding: 5px;';
			html += '">';
			html += '<img alt="' + options.text + '" src="' + options.imageSrc + '" style="';
		if (options.width != undefined) {
			html += 'width: ' + options.width + '; ';
		}
		if (options.height != undefined) {
			html += 'height: ' + options.height + '; ';
		}
		html += '" />';
		if (options.text != '') {
			html += ' ' + options.text;
		}
		html += '</div>';
		return html;
	}	

	
})(jQuery);